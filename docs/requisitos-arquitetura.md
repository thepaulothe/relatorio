# **Concepção e detalhamento da solução**

Nesta seção, delineamos a concepção e os detalhes da solução proposta para o projeto em questão. A solução visa atender às necessidades identificadas e alcançar os objetivos estabelecidos, abrangendo os aspectos de software e gerias do projeto.

## Requisitos gerais

Os requisitos gerais deste projeto abrangem as definições do produto em sua totalidade, contemplando todas as áreas relevantes: Software, Eletroeletrônica e Estrutura. Eles são divididos em dois grupos distintos: Requisitos Funcionais (RF) e Requisitos Não Funcionais (RNF).

### Requisitos Funcionais (RF)

Os requisitos funcionais descrevem as principais funcionalidades que o sistema deve oferecer, delineando as ações específicas que ele deve realizar para atender às necessidades dos usuários e aos objetivos do projeto. A tabela abaixo lista os requisitos funcionais identificados para o produto:

|  **Requisito**  |                                       **Descrição**                                              |
|:---------------:|:-----------------------------------------------------------------------------------------------:|
|       RF1       | O sistema deve realizar trocas parciais de água de forma automática.                             |             
|       RF2       | O sistema deve medir temperatura, pH e nível da água.                                             | 
|       RF3       | O sistema deve oferecer uma interface mobile para visualização de informações sobre o aquário.    | 
|       RF4       | O sistema deve permitir o controle da intensidade da iluminação do aquário via interface mobile.  | 
|       RF5       | O sistema deve dispersar ração no aquário de forma automática.                                    | 
|       RF6       | O sistema deve permitir agendamento de trocas parciais de água através da interface mobile.       | 
|       RF7       | O sistema deve incluir dois reservatórios: um para reposição de água e outro para remoção de água. | 
|       RF8       | O sistema deve enviar notificações de variações significativas de temperatura, pH e nível de água.|

### Requisitos Não Funcionais (RNF)

Os requisitos não funcionais descrevem atributos do sistema, como desempenho, segurança e usabilidade. Eles definem as características qualitativas que o produto deve possuir para satisfazer as necessidades dos usuários. A tabela a seguir enumera os requisitos não funcionais identificados para o produto:

|  **Requisito**  |                                       **Descrição**                                              |
|:---------------:|:-----------------------------------------------------------------------------------------------:|
|       RNF1      | A instalação do sistema deve ser simples e prática.                                               |             
|       RNF2      | O sistema deve ser capaz de monitorar a qualidade da água.                                        | 
|       RNF3      | O sistema deve controlar o pH e medir a temperatura da água.                                      | 
|       RNF4      | O sistema deve regular a quantidade de comida no aquário.                                          | 
|       RNF5      | O sistema deve ser acessível e intuitivo para os aquaristas.                                       | 
|       RNF6      | O sistema deve ser composto por dois reservatórios de 10 litros cada e um aquário principal de 30 litros. | 
|       RNF7      | O sistema deve garantir a segurança dos dados, mantendo o acesso privado apenas ao usuário.        |

Essa estrutura mais clara e organizada facilita a compreensão dos requisitos e permite uma referência rápida durante o desenvolvimento e a implementação do projeto.

## Requisitos de Software

Os requisitos de Software são relacionados as definições específicas para o produto de Software, no caso o App envolvido no monitoramento e controle do aquário. Também são divididos em RF (Req. Funcionais) e RNFs (Req. Não Funcionais) assim como os requisitos gerais do projeto.

**Tabela X: Requisitos funcionais para o produto de Software**

| Requisito | Descrição |
| :-------: | :-------: |
|     RF1     | O App deve disponibilizar o valor dos sensores de temperatura, PH e nível da água atualizados. |   
|     RF2     | O App deve conter um botão de acionamento, tanto para a alimentação quanto para TPA. |  
|     RF3     | O App deve conter um componente para controlar a intesidade da luz. |  
|     RF4     | O App deve oferecer uma sessão para agendamento de alimentação e TPA. |  
|     RF5     | O App deve oferecer a possibilidade de ativar ou desativar as notificações. |  
|     RF6     | O App pode apresentar histórico de últimas TPA realizadas e gráficos de temperatura, PH e nível da água. |   

**Tabela X: Requisitos não funcionais para o produto de Software**

| Requisito | Descrição | 
| :-------: | :-------: | 
|     RNF1     | O sistema deve atualizar os valores dos sensores (temperatura, PH e nível da água) a cada certa quantidade de tempo (a depender do sensor) |  
|     RNF2     | O sistema deve disponibilizar os dados dos sensores de forma clara e objetiva |  
|     RNF3     | O sistema deve disponibilizar os botões de acionamento e agendamento de forma prática | 
|     RNF4     | O sistema deve enviar o sinal de acionamento ao subsistema eletrônico de forma rápida e sem atraso, dado as boas condições de conexão Wifi |  
|     RNF5     | O sistema deve se comunicar com o subsistema eletrônico via protocolo MQTT |  
|     RNF6     | O front-end do sistema deve ser construído com o React Native JS |  

## Definição do Backlog

No desenvolvimento de software, o backlog é uma peça fundamental no método ágil de gerenciamento de projetos. Ele é uma lista dinâmica de todas as funcionalidades, melhorias, correções e tarefas que precisam ser implementadas em um produto de software. Essa lista é constantemente revisada, atualizada e priorizada durante todo o ciclo de vida do projeto.

No nosso projeto, vamos adotar uma abordagem que envolve a separação das demandas em duas categorias principais: features e user stories.

### Features

Funcionalidades ou conjuntos de funcionalidades que agregam valor ao produto final e que podem ser entregues de forma independente. Assim, representam conjuntos mais amplos de funcionalidades que contribuem para os objetivos gerais do projeto.

**Tabela X: Detalhamento Features**

|               Feature               |                                                      Descrição                                                      |
| :---------------------------------: | :-----------------------------------------------------------------------------------------------------------------: |
| 1 - Visualização do painel de dados |            Painel de consulta de dados dos sensores no aquário: temperatura, nível de PH e nível de água            |
|     2 - Acionamento de tarefas      | Acionamento de tarefas relacionadas ao aquário: alimentação, TPA (troca parcial de água) e controle de luminosidade |
|     3 - Agendamento de tarefas      |              Agendamento de tarefas relacionadas ao aquário: alimentação e TPA (troca parcial de água)              |
|     4 - Notificações de tarefas     |               Notificação de alertas: temperatura, nível de PH ou nível da água fora da margem ideial               |
|      5 - Histórico de tarefas       |                     Exibição do histórico de tarefas relacionadas ao aquário: TPA's e gráficos                      |

### User Stories

Descrições curtas de uma funcionalidade do ponto de vista do usuário, geralmente escritas na forma de "como um [tipo de usuário], eu quero [realizar alguma ação] para [alcançar algum objetivo]". Isso ajuda a manter o foco no valor que a funcionalidade trará para os usuários.

#### Backlog

**Tabela X: Detalhamento Backlog**

| Feature | User Storie |                                                                                     Descrição                                                                                     |
| :-----: | :---------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|    1    |     US1     |                        Como um dono do aquário, desejo visualizar a temperatura atual do aquário no painel de dados para monitorar as condições térmicas.                         |
|    1    |     US2     |                             Como um dono do aquário, desejo ver o nível de pH atual do aquário no painel de dados para monitorar a qualidade da água.                             |
|    1    |     US3     |                Como um dono do aquário, desejo acompanhar o nível atual da água no aquário no painel de dados para garantir um ambiente adequado para meus peixes.                |
|    2    |     US4     |                 Como um dono do aquário, desejo poder acionar a alimentação dos peixes através do sistema para garantir que eles sejam alimentados regularmente.                  |
|    2    |     US5     |                 Como um dono do aquário, desejo acionar o processo de troca parcial de água (TPA) através do sistema para manter a qualidade da água do aquário.                  |
|    2    |     US6     |                    Como um dono do aquário, desejo controlar a luminosidade do aquário através do sistema para criar ciclos de luz adequados para meus peixes.                    |
|    3    |     US7     |                   Como um dono do aquário, desejo agendar horários específicos para a alimentação dos peixes através do sistema para automatizar esse processo.                   |
|    3    |     US8     | Como um dono do aquário, desejo agendar trocas parciais de água (TPA) em intervalos regulares através do sistema para manter a qualidade da água do aquário de forma consistente. |
|    4    |     US9     |      Como um dono do aquário, desejo receber uma notificação quando a temperatura do aquário estiver fora da faixa ideal para que eu possa corrigir o problema rapidamente.       |
|    4    |    US10     |    Como um dono do aquário, desejo ativar ou desativar as notificações para que eu possa receber alertas quando eu desejar.    |
|    5    |    US11     |     Como um dono do aquário, desejo visualizar um histórico das últimas trocas parciais de água (TPA's) para entender como elas afetam a qualidade da água ao longo do tempo.     |
|    5    |    US12     |         Como um dono do aquário, desejo ver gráficos representando as variações de temperatura, pH e nível da água ao longo do tempo para analisar tendências e padrões.          |

#### Priorização

A priorização do Backlog é crucial para garantir que as funcionalidades mais importantes sejam entregues primeiro. Nesse cenário podemos utilizar as classificações abaixo:

- Must have: Funcionalidades Essenciais: Sem elas, o produto não funciona. São inegociáveis e devem ser priorizadas acima de tudo.

- Should have: Funcionalidades Importantes: Trazem valor significativo ao produto, mas podem ser adiadas se necessário.

- Could have: Funcionalidades Desejáveis: São ótimas para ter, mas não são essenciais. Podem ser implementadas no futuro, se o tempo e os recursos permitirem.

**Tabela X: Backlog Piorizado**

| Feature | User Storie |   Priorização    |   
| :-----: | :---------: | :------------: |
|    1    |     US1     |   *Must*    |
|    1    |     US2     |  *Must*  |
|    1    |     US3     |   *Must*    |
|    2    |     US4     |   *Must*      |
|    2    |     US5     |    *Must*         |
|    2    |     US6     |    *Must*    |
|    3    |     US7     |    *Must*         |
|    3    |     US8     | *Must* |
|    4    |     US9     |  *Should*    |
|    4    |    US10     |  *Should*  |
|    5    |    US11     |  *Could*    |
|    5    |    US12     |  *Could*   |

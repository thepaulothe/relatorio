# **Introdução**

A seção de Introdução se refere à fase 01 do ciclo de vida do projeto e
tem por objetivo apresentar, de forma refinada, o problema abordado no
projeto.

# **Detalhamento do problema**

Apresentar o problema

# **Levantamento de normas técnicas relacionadas ao problema**

Caso existam, apresentar normas técnicas diretamente associadas ao
problema abordado pela equipe.

# **Identificação de solução comerciais**

Realizar uma pesquisa de soluções já existentes no mercado. É
interessante realizar uma comparação (benchmarking) entre as soluções já
existente.

**Equipe**

![Organização da equipe](assets/images/Organizacao_da_Equipe%20-_PI2.png)

# **Objetivo geral do projeto**

Assegurar o bom andamento de um projeto e desenvolvimento, conforme
diretrizes gerais de qualidade.

# **Objetivo específicos do projeto**

Assegurar o bom andamento de um projeto e desenvolvimento, conforme
diretrizes gerais de qualidade.






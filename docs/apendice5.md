# **Apêndice 05 - Documentação de software**

## Metodologias

Esta sessão apresenta as metodologias que serão utilizadas para o desenvolvimento e implementação do produto de software. As metodologias selecionadas visam garantir a efetividade do processo de desenvolvimento, a qualidade do produto final e a satisfação dos stakeholders.

### 1. Lean Inception:

O Lean Inception foi utilizado como ponto de partida para o projeto, reunindo toda a equipe para um workshop intensivo de alinhamento. Através dessa metodologia, destrinchamos as características, objetivos e funcionalidades do produto de software, alcançando um entendimento compartilhado entre todos os stakeholders.

Resultados do Lean Inception:

- **Visão do Produto:** Uma visão clara e concisa do produto que estamos construindo, definindo seu propósito e valor para os usuários.
- **Quadro É/Não É:** Uma lista de funcionalidades que o produto deve e não deve ter, garantindo foco e priorização.
- **Personas e Jornadas do Usuário:** Definição dos perfis dos usuários e suas jornadas de interação com o produto, permitindo um design centrado no usuário.
- **Funcionalidades:** Lista detalhada das funcionalidades do produto, priorizadas de acordo com seu valor para o negócio.
- **Sequenciador:** Definição da ordem de desenvolvimento das funcionalidades, garantindo que as mais importantes sejam entregues primeiro.
- **Canvas MVP:** Definição do Produto Mínimo Viável (MVP), que representa a versão inicial do produto com as funcionalidades essenciais para atender às necessidades dos usuários.

### 2. SCRUM:

O SCRUM será a metodologia utilizada para o desenvolvimento e implementação do produto de software. O SCRUM é uma metodologia ágil e iterativa que se baseia em ciclos curtos de desenvolvimento chamados Sprints. Cada Sprint tem duração de 1 semana e foca na entrega de um conjunto específico de funcionalidades.

Ao longo da aplicação do SCRUM, teremos o apoio dos seguintes artefatos e ritos dessa metodologia:

- **Backlog do Produto:** O Backlog do Produto é a lista viva e priorizada de todas as funcionalidades que o produto deve ter. Ele serve como um guia para a equipe, definindo o que deve ser desenvolvido e em qual ordem. 

- **Dailys:** As dailys são alinhamentos diários de segunda a sábado para reportar o status e andamento das atividades, para que se estabelece uma comunicação eficiente e próxima entre o time.

- **Sprint Planning:** No Sprint Planning, a equipe se reúne para definir as funcionalidades que serão implementadas no próximo Sprint, com um entendimento claro das metas e dos objetivos a serem alcançados. 

- **Sprint Review:** Ao final de cada Sprint, a equipe realiza a Sprint Review. Nessa reunião, os desenvolvedores apresentam as funcionalidades concluídas para identificar oportunidades de melhoria e garantir que o produto esteja no caminho certo para atender às expectativas dos usuários.

- **Sprint Retrospective:** A Sprint Retrospective é um momento de reflexão e aprendizado para a equipe. Nessa reunião, os membros da equipe discutem o que deu certo e o que deu errado no Sprint anterior, identificando áreas de melhoria para os próximos Sprints.

Os benefícios esperados ao utilizar o SCRUM englobam a Maior Adaptabilidade, pois permite adaptações ao longo do projeto, além da Entrega Contínua, que permite a entrega de valor aos usuários de forma incremental, permitindo feedback e validação precoces.

### 3. Pair Programming - XP

Inspirados na filosofia do Extreme Programming (XP), será adotado a prática de Pair Programming no processo de desenvolvimento. Nesta prática, os desenvolvedores trabalham em pares, compartilhando a mesma estação de trabalho e colaborando na escrita de código.

Os benefícios esperados na aplicação do Pair Programming são o Maior Compartilhamento de Conhecimento e principalmente a Melhor Resolução dos Problemas.

### 4. Quadro Kanban

O Quadro Kanban será utilizado para o gerenciamento das atividades do grupo de software. O Kanban é uma metodologia visual que organiza as tarefas em colunas de acordo com seu status (Aberto, Em Progresso, Fechado).

Os benefícios esperados na aplicação do Quadro Kanban no projeto são a Maior Visibilidade do Trabalho e Melhoria no Fluxo de Trabalho.

## Diagrama de pacotes

Um diagrama de pacotes é um tipo de diagrama de estrutura usado em engenharia de software para visualizar a arquitetura de um sistema. Ele mostra como os diversos componentes ou pacotes de um sistema estão organizados e interagem entre si. Os pacotes representam grupos lógicos de elementos do sistema, como classes, interfaces, subpacotes, entre outros.

### Camada ReactJs Front End:
- **Components**: Este pacote contém os componentes reutilizáveis que são usados para construir a interface do usuário (UI). Esses componentes podem incluir botões, formulários, barras de navegação, etc.
   
- **Pages**: Aqui estão as diferentes páginas da aplicação. Cada página representa uma visualização específica que o usuário pode interagir. Por exemplo, a página da painel de visualização, página de acionamento de tarefas, página de agendamento, etc.
   
- **Assets**: Esse pacote armazena recursos estáticos, como imagens, fontes, ícones, etc., que são utilizados na construção da interface do usuário.
   
- **CSS**: Neste pacote, estão os arquivos de estilo que definem a aparência visual dos componentes e páginas da aplicação.
   
- **Services**: Aqui estão os serviços que encapsulam a lógica de negócios da aplicação. Isso pode incluir serviços para fazer requisições ao Broker MQTT, manipular dados locais, enviar sinais de acionamento, etc.
   
- **Routes**: Este pacote contém as definições de roteamento da aplicação. Ele mapeia URLs para componentes ou páginas específicas, permitindo a navegação dentro da aplicação React.

### Camada de dados:
- **Memória Cache Dispositivo**: Este pacote representa a camada de armazenamento em cache local, que é usada para armazenar temporariamente dados frequentemente acessados para melhorar o desempenho da aplicação. Pode incluir caches de objetos, caches de consultas, etc.

- **Service de Configuração**: Aqui reside o serviço responsável pela configuração da aplicação. Ele pode lidar com a leitura e escrita de configurações, gerenciamento de parâmetros de ambiente, inicialização de componentes, entre outros.

### Broker MQTT:
- **Tópicos**: Este pacote representa os tópicos de mensagens no protocolo MQTT. Os tópicos são canais aos quais os clientes se inscrevem para receber ou publicar mensagens. Eles são usados para organizar a comunicação entre os diferentes dispositivos ou serviços.

- **Clients**: Aqui estão os clientes MQTT que se conectam ao broker. Cada cliente representa um dispositivo ou aplicação que pode publicar ou subscrever a tópicos para trocar mensagens com outros clientes.

- **Comunicação**: Este pacote engloba os mecanismos de comunicação utilizados pelo broker MQTT para facilitar a troca de mensagens entre os clientes. Isso pode incluir protocolos de rede, gerenciamento de sessões, autenticação, etc.

![Diagrama de pacotes](https://github.com/fga-eps-mds/A-Disciplina-MDS-EPS/assets/51385738/4f207b92-373a-4be9-93f6-8982c85f898a)


## Diagrama de Casos de Uso

A seção seguinte apresentará um diagrama de caso de uso, delineando as principais interações entre os usuários e o sistema Aquamatico. Este diagrama servirá como um guia visual para compreender como diferentes partes interessadas interagem com o sistema e quais funcionalidades estão disponíveis para cada uma delas. Essa representação gráfica será uma ferramenta valiosa para o desenvolvimento e a comunicação eficaz ao longo do ciclo de vida do projeto.

### Descrição

Cuidar de um aquário é uma tarefa que parece simples, mas é trabalhosa. No aquário, várias coisas afetam o equilíbrio, como pH, temperatura, amônia, nitrito e nível da água. Por exemplo, o pH determina quais peixes e plantas podem viver lá. Testamos esses valores e, se estiverem errados, tomamos medidas, como resfriar se estiver quente demais ou adicionar produtos químicos para ajustar o pH. É um trabalho que exige atenção constante para manter o ambiente saudável para os habitantes do aquário.

### Diagrama Casos de Uso

![Diagrama Casos de Uso](assets/images/diagrama_casos_de_uso.jpg)

## Atores

- **Aquarista**: Este ator interage diretamente com o sistema para realizar tarefas como monitorar parâmetros da água, gerenciar alimentação, ajustar a intensidade de luz, entre outras.
- **Sistema Automatizado**: Representa o sistema automatizado instalado no aquário, responsável por realizar determinadas tarefas de forma autônoma. Este ator interage com o aquário de forma automatizada, executando funções de controle e monitoramento conforme configurado pelo usuário.

## Relacionamentos

O relacionamento delineado por esta linha tracejada no diagrama é um relacionamento de Extensão. Um relacionamento de extensão serve para especificar que um caso de uso (extensão) estende o comportamento de outro caso de uso (base).

## Histórico de Revisão

| Data | Descrição| Autor  |
|------------|----------------------------|---------|
| 25/04/2024 | Versão inicial   | Henrique Hida |

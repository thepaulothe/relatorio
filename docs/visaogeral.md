# **Introdução**

O projeto Aquamático surge da necessidade de automatizar a manutenção e alimentação de animais aquáticos, visando garantir precisão e cuidado contínuo com esses seres vivos. Com a crescente demanda por soluções que facilitem a rotina dos cuidadores, especialmente em um contexto em que a sociedade busca maneiras de otimizar o tempo e garantir o bem-estar dos animais, o Aquamático se apresenta como uma resposta eficaz a esses desafios.

Dada a possibilidade dos tutores desses animais serem acometidos por esquecimento de alguns cuidados, diante da suas demandas, e falhas na quantidade de comida a se dada e limpeza a serem realizadas, o Aquamático é uma solução que pode apoiar nisso através das suas automações, a exemplo de alimentação automática e monitoramento geral das condições do ambiente. 

Nesse sentido, essa solução foi concebida para oferecer uma maneira eficiente e precisa de cuidar dos animais aquáticos, garantindo uma vida saudável e segura para eles. Ao automatizar tarefas como trocas parciais de água, medição de parâmetros vitais e dispersão de alimentos, o Aquamático visa proporcionar aos cuidadores a tranquilidade de saber que seus animais estão sendo cuidados de forma adequada, mesmo em sua ausência.

Assim, o Aquamático destaca-se como uma solução inovadora que agrega valor à sociedade, promovendo o bem-estar dos animais aquáticos e facilitando a rotina dos cuidadores. Ao oferecer uma abordagem automatizada e precisa para a manutenção e alimentação de animais aquáticos, o projeto visa atender às necessidades crescentes desse mercado, contribuindo para uma convivência harmoniosa entre os tutores e animais aquáticos.

# **Detalhamento do problema**

Manter um aquário é uma tarefa desafiadora e ao mesmo tempo recompensadora, simular um ambiente aquático de forma natural em um cômodo é algo que requer bastante controle. Apesar de todos os desafios, a tarefa de cuidar de um aquário é simples porém trabalhosa.

No aquário existem diversas variáveis que influenciam no desenvolvimento e equilíbrio de um sistema. Por exemplo: ph, temperatura, amônia, nitrito e nível da água. A influência do ph em um aquário irá definir quais espécies de peixes, invertebrados e plantas o aquário poderá ter. 

Quando realizamos testes desses parâmetros citados acima, dependendo dos valores obtidos e esperados devem ser tomadas certas providências. Por exemplo se a temperatura medida for acima do esperado, deve realizar a resfriamento do aquário. Se caso o ph esperado for acima de 7 e quando medido for próximo de 6 então deve ser adicionado um químico que aumente a reserva alcalina. 

Diante disso, o nosso projeto irá abordar as seguintes questões:

- Monitorar parâmetros da água: Os parâmetros que serão medidos serão ph, temperatura e nível de água. Com essas informações será possível saber qual ação deverá ser tomada para retornar para as condições esperadas.

- Gerenciar alimentação: Será possível marcar de quantas em quantas horas deverá ser liberado a alimentação para os habitantes do aquário. Dessa forma, será possível evitar sobras de alimentos que acabam influenciando na qualidade da água.

- Gerenciar intensidade de luz: A intensidade de luz e o tempo que ela está ligada em um aquário deve ser controlado para não causar estresse na fauna encontrada no aquário e precaver o surgimento de algas indesejadas.

- Gerenciar nível de água: A água presente em um aquário evapora com o dercorrer do tempo. Além disso semanalmente é necessário a realização de trocas parciais de água a fim de manter a qualidade da água.

Ao abordar esses desafios, a equipe irá conseguir aplicar de forma sólidas os aprendizados adquiridos ao longo da sua graduação em engenharia.

# **Levantamento de normas técnicas relacionadas ao problema**

Caso existam, apresentar normas técnicas diretamente associadas ao
problema abordado pela equipe.

# **Identificação de solução comerciais**

Realizar uma pesquisa de soluções já existentes no mercado. É
interessante realizar uma comparação (benchmarking) entre as soluções já
existente.

**Equipe**

![Organização da equipe](assets/images/Organizacao_da_Equipe%20-_PI2.png)

# **Objetivo geral do projeto**

O projeto busca criar uma automação de um aquário que torne a tarefa de manter um aquário mais prática. Dessa forma, dispondo de informações importantes como temperatura, ph e nível de água além de também controlar nível de água, alimentação, intensidade da luz e trocas parciais de água.

# **Objetivo específicos do projeto**

- Projetar e desenvolver um sistema de automação de um aquário que seja confiável, prático e de baixo custo;

- Integrar sensores de temperatura, ph e nível de água;

- Desenvolver algoritmos que controlem a troca parcial de água e também a alimentação dos peixes;

- Criar uma interface mobile que seja acessível e amigável onde seja possível visualizar dados de temperatura, ph, nível de água além de controlar a intensidade da iluminação, a alimentação dos peixes e a troca parcial de água;

- Garantir a coleta contínua de dados do aquário em tempo real a fim de manter o conrtrole de parâmetros;

- Documentar todo o processo de desenvolvimento e resultados coletados.

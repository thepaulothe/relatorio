# **Apêndice 01 - Aspectos de gerenciamento do projeto**

## Termo de abertura do projeto

### Título do projeto

**Aquamático**

### Descrição do projeto

Este projeto consiste na automação de um aquário. O sistema ficará conectado por sensores de pH, temperatura e nível de água, além de ter uma luminária para controle de intensidade de iluminação e um alimentador automático. Também incluirá dois reservatórios dispostos, um para reposição de água e outro para remoção da água do aquário. Além disso, o projeto contará com uma aplicação móvel onde o usuário conseguirá controlar a alimentação dos peixes, a intensidade da iluminação e a troca parcial de água, este será realizado do dia 18/03/2024 a 15/07/2024.

### Justificativa do projeto

Manter um aquário é uma atividade complexa, pois estamos lidando com um ambiente sensível a diversos parâmetros, como temperatura, amônia, nitrito e pH. No entanto, também pode ocorrer erros humanos, como o excesso de alimentação ou descartar mais água do que o necessário, o que pode desencadear desequilíbrios dentro do aquário.

### Objetivo do projeto

O objetivo geral deste projeto é desenvolver um sistema de automação para um aquário. Os objetivos específicos incluem a coleta de dados como pH, temperatura e nível de água, fornecer um alimentador automático onde o usuário consiga programar a liberação de comida para os peixes, controlar a intensidade da iluminação do aquário e realizar a troca parcial de água de forma automática. Além disso, todas essas funcionalidades devem estar disponíveis através de uma aplicação móvel.

### Responsabilidades e autoridades do gerente de projeto

Yan Andrade de Sena atua como Coordenador Geral no projeto, sendo responsável por liderar a equipe, garantir o cumprimento do escopo, prazos e orçamento, coordenar as atividades, minimizar os riscos e manter uma comunicação eficaz. Ele tem a autoridade para tomar decisões que impactam diretamente o projeto e representa a equipe perante os stakeholders.

### Stakeholders

#### Professores orientadores

Os professores orientadores têm um papel essencial na supervisão e orientação deste projeto. Sua contribuição inclui acompanhar o progresso, oferecer orientações técnicas, insights estratégicos e realizar avaliações críticas. Sua expertise é fundamental para garantir a qualidade e eficácia do sistema de automação do aquário, fornecendo uma orientação valiosa para a equipe.

#### Graduandos de engenharia

Os graduandos de engenharia formam a equipe executora deste projeto. Eles são responsáveis pela implementação das soluções propostas, desenvolvimento do sistema, integração dos componentes e garantia da funcionalidade adequada. Sua dedicação, habilidades técnicas e colaboração são elementos cruciais para alcançar com sucesso os objetivos e entregas deste projeto.

#### Usuários

Os usuários finais deste projeto são pessoas que têm o aquarismo como hobby ou até mesmo pessoas lojistas de aquarismo. A experiência e o feedback deles são essenciais para validar e aprimorar continuamente o sistema. A interação com os aquaristas é fundamental para assegurar que o sistema atenda às suas necessidades, otimize os recursos disponíveis e melhore a qualidade de vida dos peixes.

## Orçamento estimativo

Esta seção destina-se a fornecer uma estimativa detalhada e fundamentada do orçamento geral do projeto, visando garantir uma gestão financeira eficaz ao longo do projeto,de forma a viabilizar sua realização.


|                   Componente                    |     Quantidade     |     Valor    |
|:-----------------------------------------------:|:------------------:|:------------:|
|     ESP-32 wroon                                |          1         |     45,60    |
|     Arduino Uno                                 |          1         |     50,00    |
|     Fonte DC 9V 1A modelo LJH-186               |          1         |     25,00    |
|     Sensor de temperatura modelo DS18B20        |          1         |     51,85    |
|     Regulador de tensão modelo LM7805           |          1         |     2,40     |
|     Motor de passo modelo 28BYJ-48              |          1         |     23,00    |
|     Módulo do motor driver ULN2003              |          1         |     22,00    |
|     Sensor de ph modelo eletrodo-bnc            |          1         |     189,00   |
|     Módulo do sensor de ph PH4502C              |          1         |       -      |
|     Sensor de nível modelo WSF1-E4              |          1         |     28,05    |
|     Bomba de água para filtragem modelo XT-300  |          1         |     40,00    |
|     Luminária modelo DB-0008                    |          1         |     40,00    |
|     Resistores 1O KΩ                            |         14         |     9,80     |
|     Resistores 4,7 KΩ                           |          1         |     0,05     |
|     Resistores 1 KΩ                             |          1         |     0,05     |
|     Tranistor modelo MOSFET-IRFZ44N             |          2         |     18,00    |
|     Capacitores 0.1 uF                          |          2         |     0,84     |
|Módulo relé 4 canais modelo Songle SRD-05VDC-DL-C|          1         |     24,90    |
|     Diodos de proteção modelo 1N4007            |          4         |     1,76     |
|     Mini bomba de água submersível 3V-5V        |          2         |     25,92    |
|     Cantoneira Dobrada 25x25 e1,50 (mm) [3m]    |          2         |     31,12    |
|     Cantoneira Dobrada 22x22 e1,50 (mm) [3m]    |          1         |     11,83    |
|     Degrau de Madeira (1x1m)                    |          1         |     40,00    |
|     Mão de Obra Madeira                         |          -         |     50,00    |
|     Chapa ACM branco brilhoso 1220X5000MM 3MM   |          1         |    340,73    |
|     Selante Adesivo 420g                        |          1         |     50,76    |
|     Parafuso Allen Abaulado MA 6 X 25           |         14         |     42,54    |
|     Dobradiça Gonzo                             |          2         |     12,00    |
|     Cola de Madeira (250g)                      |          2         |     35,98    |
|     Porca 1/4 (1 fio)                           |         14         |     4,20     |
|     Arruela 6mm                                 |         14         |     4,20     |
|     Impressão 3D                                |       7h-10h       |     140,00   |
|     Mangueira Sakura p/ Aquário 2m              |         1          |     17,00    |
|     Fixador de fio                              |         1          |     22,00    |
|     Puxador de Porta 128mm                      |         1          |     29,90    |

Desta forma, após uma análise dos requisitos e elementos do projeto Aquamatico, estima-se que o orçamento necessário para a sua realização completa seja de R$ 1.394,50 (mil trezentos e noventa e quatro reais e cinquenta centavos).

## **Lista É / Não É**

Relação do que o produto, ou subproduto, é, e do que o produto, ou subproduto, não é.

Este processo é necessário para restringir ao seu mínimo o escopo do projeto, garantindo um melhor foco.

## Organização da equipe

A equipe está organizada da seguinte forma:

![Organização da equipe](assets/images/Organizacao_da_Equipe%20-_PI2.png)

## Repositórios

> Apresentar links para acesso aos repositórios do projeto.

## EAP (Estrutura Analítica de Projeto) Geral do Projeto

É com base na técnica de decomposição que se consegue dividir os principais produtos do projeto em nível de detalhe suficiente para auxiliar a equipe de gerenciamento do projeto na definição das atividades do projeto.

### EAP do subsistema 01

Inserir a EAP de cada um dos subsistemas que compõem o projeto.

### EAP do subsistema 02

Inserir a EAP do subsistema 02.

<<<<<<< HEAD
### Definição de atividades e backlog das sprints
=======
### Definição de atividades e cronograma de execução

O cronograma de execução visa garantir uma execução eficiente e organizada, este estabelece as tarefas a serem realizadas e os prazos para sua conclusão e os responsáveis por cada etapa do processo. Neste contexto, o presente cronograma de atividades visa garantir a eficiência no processo de planejamento e execução do projeto.

|                   Atividades                 | Data de início |  Data de entrega |
|:--------------------------------------------:|:--------------:|:----------------:|
|     *PC1*                                    |     20/03      |       10/05      |
|     Definição do projeto                     |     22/03      |       27/03      |
|     Definição da equipe                      |     27/03      |       27/03      |
|     Definição dos requisitos                 |     27/03      |       03/04      |
|     Escrita do relatório                     |     03/04      |       05/05      |
|     Entrega do PC1                           |     20/03      |       10/05      |
|     Entrega do relatório                     |     05/05      |       05/05      |
|     Apresentação do PC1                      |     10/05      |       10/05      |
|     Compra de materiais                      |     06/05      |       17/05      |  
|     Construção do subsistema de estrutura    |     06/05      |       17/05      |  
|     Testes  do subsistema de estrutura       |     06/05      |       17/05      |  
|     Construção do subsistema de eletrônica   |     06/05      |       27/05      |  
|     Testes  do subsistema de eletrônica      |     06/05      |       27/05      |  
|     Construção do subsistema de energia      |     06/05      |       27/05      |  
|     Testes  do subsistema de energia         |     06/05      |       27/05      |  
|     Construção do subsistema de software     |     06/05      |       27/05      |  
|     Testes  do subsistema de software        |     06/05      |       27/05      |  
|     Integração dos subsistemas               |     27/05      |       07/06      |  
|     Testes                                   |     27/05      |       07/06      |  
|     Escrita do relaório                      |     06/05      |       09/06      |
|     Entrega do relatório                     |     09/06      |       09/06      |  
|     Entrega do PC2                           |     14/06      |       14/06      |  
|     Ajustes finais do projeto                |     14/06      |       05/07      |  
|     Entrega do PC3                           |     05/07      |       05/07      |  
>>>>>>> ebfff1dede0138ec27f6307df24ee0464b4c2394

